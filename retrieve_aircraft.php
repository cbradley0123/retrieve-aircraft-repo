<?php

/*  This program shows reading a large json file, parsing the file for useful data, using the data to call other APIs to
   get additional data, then after parsing the additional data writing to a MySQL db.

    This program reads all of the aircraft currently recorded on a Raspberry Pi running PiAware ADS-B aircraft tracking
   software.  Information is then supplemented by calls to flightradar and adsbexchange.  These calls supplement the
   information by looking for the registration of the aircraft along with any airline associated with it.  Further, the
   squawk and callsign are looked up.
*/


// Has the login credentials for MySQL db
require_once('/conf/db_info.conf');


// Create PDO db connection
$dsn = "mysql:host=$db_host;dbname=$db_schema";
$pdo = new PDO($dsn, $db_user, $db_pwd);


function get_adsb_receiver_data() {
    $resp = file_get_contents('http://' . $web_address . '/data/aircraft.json');
    $raw_resp = json_decode($resp);
    return $raw_resp->aircraft;
}

function update_hex_code($adsb_aircraft, $pdo) {
    $hex_sql = "select hex_code, model_code, registration from bha.AIRPLANE where hex_code = ? order by last_seen desc limit 1";
    $stmt = $pdo->prepare($hex_sql);

    $insert_sql = "insert into bha.AIRPLANE (hex_code, LAST_SEEN) values (?, now())";
    $insert_stmt = $pdo->prepare($insert_sql);
    
    $update_sql = "update bha.AIRPLANE set last_seen = NOW() where hex_code = ? and model_code = ? and registration = ?";
    $update_stmt = $pdo->prepare($update_sql);

    foreach ($adsb_aircraft as $aircraft) {
        $hex = strtoupper(preg_replace("/[^A-Za-z0-9]/", '', $aircraft->hex));

        $stmt->execute([$hex]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            $update_stmt->execute([$hex, $row['model_code'], $row['registration']]);
        } else {
            $insert_stmt->execute([$hex]);
        }
    }
}

function  get_db_hexes($pdo) {
    $hex_sql = "select hex_code, model_code, registration from bha.AIRPLANE where last_seen > DATE_SUB(NOW(),INTERVAL 15 MINUTE)";
    $stmt = $pdo->prepare($hex_sql);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $db_hexes = [];
    foreach($results as $result) {
        $db_hexes[$result['hex_code']] = $result;
    }
    return $db_hexes;
}

function get_adsbexchange_data($hex_array, $curl) {
    //Set the secure key for making requests to adsbexchange      
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("api-auth:". $auth_key));
    
    $hexs = array_keys($hex_array);
    foreach($hexs as $hex) {        
        $url = 'https://adsbexchange.com/api/aircraft/icao/' . strtoupper($hex);

        //Set the URL that we want to send our POST request to.
        curl_setopt($curl, CURLOPT_URL, $url);

        //Execute the login request.
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        if (! $resp) {
            continue;
        }

        // check to make sure we have an aircraft return type
        if (! array_key_exists('ac', $resp)) {
            continue;
        }
        if ($resp->ac){
            $hex_array[$hex]['registration'] = $resp->ac[0]->reg;
            $hex_array[$hex]['model_code'] = $resp->ac[0]->type;
            $hex_array[$hex]['squawk'] = $resp->ac[0]->sqk;
            $hex_array[$hex]['callsign'] = $resp->ac[0]->call;
        }
    }
    return $hex_array;
}

function get_flightradar_data($hex_array, $curl) {
    $hexs = array_keys($hex_array);
    foreach($hexs as $hex) {
        $url = 'https://api.flightradar24.com/common/v1/search.json?fetchBy=reg&query=' . strtoupper($hex);

        //Set the URL that we want to send our POST request to.
        curl_setopt($curl, CURLOPT_URL, $url);

        //Execute the login request.
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        if (! $resp) {
            continue;
        }
        $aircraft = $resp->result->response->aircraft->data[0];
        if (! $aircraft) {
            continue;
        }

        if (array_key_exists('registration', $aircraft)) {
            $hex_array[$hex]['registration'] = $aircraft->registration;
        }
        if (array_key_exists('model', $aircraft)) {
            $hex_array[$hex]['model_code'] = $aircraft->model->code;
            $hex_array[$hex]['model_text'] = $aircraft->model->text;
            
        }
        if ($aircraft->airline) {
            $hex_array[$hex]['airline_name'] = $aircraft->airline->name;
            $hex_array[$hex]['airline_iata'] = $aircraft->airline->code->iata;
            $hex_array[$hex]['airline_icao'] = $aircraft->airline->code->icao;
        }
    }
    return $hex_array;
}

function prep_aircraft($db_hex_array) {

    // set default values so that the db doesnt complain about missing arguments
    $aircraft_params = array("registration"=>null, "squawk"=>null, "callsign"=>null, "model_code"=>null,
    "model_text"=>null, "airline_name"=>null, "airline_iata"=>null, "airline_icao"=>null);
    $hex_array = [];

    // whatever parameters we have, but them in the aircraft_param list
    foreach ($db_hex_array as $hex) {
        $hex_array[$hex] = $aircraft_params;
    }
    return $hex_array;
}

function write_aircraft_data($hex, $aircraft, $db_craft, $upsert_stmt, $update_stmt, $select_stmt) {
    if ($aircraft['airline_name'] == null) {
        $airline_id = null;
    } else {
        $select_stmt->execute([$aircraft['airline_name']]);
        $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
        $airline_id = $row['airline_id'];
    }

    if ($db_craft['model_code'] == '' and $db_craft['registration'] == '' and $aircraft['registration'] != null
    and $aircraft['model_code'] != null) {
        $update_stmt->execute([$aircraft['registration'], $aircraft['model_code'], $airline_id, $hex, '', '']);
    } else {
        $upsert_stmt->execute([$hex, $aircraft['registration'], $airline_id, $aircraft['model_code'], $airline_id]);
    }
}

function write_callsign_data($hex, $aircraft, $select_stmt, $update_stmt, $insert_stmt) {
    if ($aircraft['callsign'] == null) { // nothing to write
        return;
    }
    $select_stmt->execute([$hex]);
    $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
    if ($row) {
        if ($aircraft['callsign'] == $row['callsign']) {
            $update_stmt->execute([$row['record_id']]);
        } else {
            $insert_stmt->execute([$hex, $aircraft['callsign']]);
        }
    } else {
        $insert_stmt->execute([$hex, $aircraft['callsign']]);
    }
}

function write_model_data($aircraft, $select_stmt, $update_stmt, $insert_stmt) {
    if ($aircraft['model_code'] == null) {
        return;
    }
    $select_stmt->execute([$aircraft['model_code']]);
    $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
    if ($row) {
        if ($aircraft['model_text'] == null) {
            return;
        }
        if ($row['model_text'] == $aircraft['model_text']) {
            return;
        }
        $update_stmt->execute([$aircraft['model_text'], $aircraft['model_code']]);
    } else {
        $insert_stmt->execute([$aircraft['model_code'], $aircraft['model_text']]);
    }
}

function write_airline_data($aircraft, $select_stmt, $update_stmt, $insert_stmt) {
    if ($aircraft['airline_name'] == null) {
        return;
    }
    $select_stmt->execute([$aircraft['airline_name']]);
    $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
    if ($row) {
        if ($aircraft['airline_iata'] == null and $aircraft['airline_icao'] == null) {
            return;
        }
        if ($aircraft['airline_iata'] == $row['aircraft_iata'] and $aircraft['airline_icao'] == $row['aircraft_icao']) {
            return;
        }
        if ($aircraft['airline_iata'] == null) {
            $aircraft['airline_iata'] = $row['airline_iata'];
        }
        if ($aircraft['airline_icao'] == null) {
            $aircraft['airline_icao'] = $row['airline_icao'];
        }
        $update_stmt->execute([$aircraft['airline_iata'], $aircraft['airline_icao'], $row['airline_id']]);
    } else {
        $insert_stmt->execute([$aircraft['airline_name'], $aircraft['airline_iata'], $aircraft['airline_icao']]);
    }
}

function write_all_data($hex_array, $db_aircraft, $pdo) {
    # Airline data queries
    $airline_select_sql = "Select airline_id, airline_name, airline_iata, airline_icao from bha.AIRLINES where airlines_name = ? limit 1";
    $airline_select_stmt = $pdo->prepare($airline_select_sql);

    $airline_update_sql = "Update bha.AIRLINES set airline_iata = ?, airline_icao = ? where airline_id = ?";
    $airline_update_stmt = $pdo->prepare($airline_update_sql);

    $airline_insert_sql = "Insert into bha.AIRLINES (airline_name, airline_iata, airline_icao) VALUES (?,?, ?)";
    $airline_insert_stmt = $pdo->prepare($airline_insert_sql);

    #Model data queries
    $model_select_sql = "Select model_code, model_text from bha.MODELS where model_code = ?";
    $model_select_stmt = $pdo->prepare($model_select_sql);

    $model_update_sql = "Update bha.MODELS set model_text = ? where model_code = ?";
    $model_update_stmt = $pdo->prepare($model_update_sql);

    $model_insert_sql = "Insert into bha.MODELS (model_code, model_text) VALUES (?,?)";
    $model_insert_stmt = $pdo->prepare($model_insert_sql);

    #Callsign data queries
    $callsign_select_sql = "SELECT record_id, callsign from bha.CALLSIGNS where hex_code = ? order by last_seen DESC limit 1";
    $callsign_select_stmt = $pdo->prepare($callsign_select_sql);

    $callsign_update_sql = "Update bha.CALLSIGNS set last_seen = NOW() where record_id = ?";
    $callsign_update_stmt = $pdo->prepare($callsign_update_sql);

    $callsign_insert_sql = "Insert into bha.CALLSIGNS (hex_code, callsign) VALUES (?,?)";
    $callsign_insert_stmt = $pdo->prepare($callsign_insert_sql);

    #Aircraft data queries
    $aircraft_upsert_sql = "INSERT INTO bha.AIRPLANE
                    (hex_code, registration, airline_id, model_code)
                    VALUES(?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE
                    airline_id = ?";
    $aircraft_upsert_stmt = $pdo->prepare($aircraft_upsert_sql);

    $aircraft_update_sql = "Update bha.AIRPLANE set registration = ?, model_code = ?, airline_id = ? where hex_code = ? and registration = ? and model_code = ?";
    $aircraft_update_stmt = $pdo->prepare($aircraft_update_sql);

    $aircraft_select_sql = "Select airline_id from bha.AIRLINES where airline_name = ?";
    $aircraft_select_stmt = $pdo->prepare($aircraft_select_sql);

    $hex_numbers = array_keys($hex_array);
    foreach($hex_numbers as $hex) {
        $db_craft = $db_aircraft[$hex];
        $aircraft = $hex_array[$hex];
        write_model_data($aircraft, $model_select_stmt, $model_update_stmt, $model_insert_stmt);
        write_airline_data($aircraft, $airline_select_stmt, $airline_update_stmt, $airline_insert_stmt);
        write_aircraft_data($hex, $aircraft, $db_craft, $aircraft_upsert_stmt, $aircraft_update_stmt, $aircraft_select_stmt);
        write_callsign_data($hex, $aircraft, $callsign_select_stmt, $callsign_update_stmt, $callsign_insert_stmt);
    }
}


 if (isset($argv[1])) { 
        if ($argv[1] == "regular" or $argv[1] == 'quarterly') {
            $program_to_run = $argv[1];
        } else {
            die("Invalid option\n");
        }
    } else { 
        die("no qualifying argument!\n"); 
    }

//Initiate cURL.
$curl = curl_init();
//Tells cURL to return the output once the request has been executed.
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
$time_start = microtime(true);
echo "\n" . str_repeat("*", 20) . "\n" . date("m-d-Y H:i:s") . "\nStarting " . $program_to_run . " run\n";
echo "Getting receiver info...\n";
$adsb_aircraft = get_adsb_receiver_data();
echo "Updating hex codes...\n";
update_hex_code($adsb_aircraft, $pdo);
if ($program_to_run == 'quarterly') {
    echo "Getting_db_hexes...\n";
    $db_aircraft = get_db_hexes($pdo);
    echo "Prepping aircraft...\n";
    $hex_array = prep_aircraft(array_keys($db_aircraft));
    echo "Getting flightradar24 info...\n";
    $hex_array = get_flightradar_data($hex_array, $curl);
    echo "Getting adsbexchange info...\n";
    $hex_array = get_adsbexchange_data($hex_array, $curl);
    $db_time_start = microtime(true);
    echo "Writing data....\n";
    write_all_data($hex_array, $db_aircraft, $pdo);
    
}
$time_end = microtime(true);
echo "\nRun complete\nRun time: " . number_format($time_end - $time_start, 2) . " seconds\n";
if ($program_to_run == 'quarterly') {
    echo "DB write time: " . number_format($time_end - $db_time_start, 2) . " seconds\n"; 
}
echo str_repeat("*", 20) . "\n";