# Retrieve Aircraft Repo

This is an example of my PHP code.  The purpose here is to demonstrate usage of API calls, JSON data, using the JSON data for further API calls, and finally writing to a MySQL db.

To flesh this out a little, we start off by getting a JSON dump of all of the aircraft currently being recorded by an ADS-B receiver running on a Raspberry Pi.  
After the initial list is received, API calls are made to Flight Radar where further information is retrieved, such as Airline information.  Another API call to 
adsbexchange attempts to find aircraft squawk code and callsign information.  Once all of this information is gathered, it is written to a MySQL db for later retrieval.

----- Licence ------

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy 
of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses.